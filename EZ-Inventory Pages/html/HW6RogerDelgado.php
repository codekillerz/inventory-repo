<!DOCTYPE html>
<!--HW6RogerDelgado php file NBA TEAM ID search Form  -->
<html lang = "en">
   <head>
<title> PHP Script for Teams Table </title>
<link rel="stylesheet" type="text/css" href="HW6RogerDelgado.css">
   </head>
<body>
<?php
// Create connection
$db = mysqli_connect("localhost","delga051","testing123");

if (!$db) // Check connection if statement
  {
    print "*****CONNECTION TO DATABASE FAILED***** ";exit;
  } 
echo "*****CONNECTION TO DATABASE SUCCESSFUL*****";

$delga051 = mysqli_select_db($db, "delga051");

if (!$delga051)//if statement checks the database selected
  {
    print "Error - unable to select from this database.";exit;
  }

// Get the teamID entered by the user in the ID box
$teamID = $_POST['TeamID'];
$teamID = stripcslashes($teamID);
$teamIDU = htmlspecialchars($teamID);

//query to select info from NBA team
$query = "SELECT * FROM Teams WHERE TeamID = '$teamIDU'";

//Variable to store query result
$Qresult = mysqli_query($db, $query);

if(!$Qresult)//if statement for checking query
  {
  print "Error - The query was not successful.";
  $error = mysql_error();//sql error
  print "<p>" . $error . "</p>";
  exit;
 }

// Display the results in a table
print "<div>";
print "<table> <caption> <h1> NBA Team Info: </h1>";
print" <h4> Team ID Entered: " . $teamIDU . "</h4></caption>";
print "<tr align = 'center'>";


$n_rows = mysqli_num_rows($Qresult);// Number of rows var

if($n_rows > 0)
{
  // Get the first row of the result
  $firstRow = mysqli_fetch_assoc($Qresult);
  
  // Get the number of fields in the result
  $num_fields = mysqli_num_fields($Qresult);
  
  // Get the column names
  $CNames = array_keys($firstRow);
  
  // Display column names as appropriate column headers
  for($i = 0; $i < $num_fields; $i++)
    {
      print "<th>" . $CNames[$i] . "</th>";
    }
  print "</tr>";
  
  // Display the values of rows
  for($row_num = 0; $row_num < $n_rows; $row_num++)
    {
      print "<tr>";
      $values = array_values($firstRow);      
      for($i = 0; $i < $num_fields; $i++)
	{
	  $value = htmlspecialchars($values[$i]);
	  print "<td>" . $value . "</td>";
	}
      print "</tr>";// End row
      $firstRow = mysqli_fetch_assoc($Qresult);//next row
    }
 }
 else//else statement if nothing is inputed or false
   {
     print "<td> *****ERROR: USER INPUT INVALID TeamID FROM Teams TABLE*****.</td> <br />";
   }

// Close table
print "</table>";
print "</div>";
?>
</body>
</html> 